<?php


class Helper{
	
	public static $RateList = array();
	public static $ProductList = array();
	public static $CategorySortedList = array();
	
	public static function getRate ($q){	

		if(!array_key_exists($q, Helper::$RateList)){
			$url  = "http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in (\"$q\")&env=store://datatables.org/alltableswithkeys";
			$rate = simplexml_load_file($url); 
			Helper::$RateList[$q] = (string)$rate->results->rate->Rate;
		}
		return Helper::$RateList[$q];
	}

	public static function Zip($source, $destination)
	{
		// Get real path for our folder
		$rootPath = realpath($destination);

		// Initialize archive object
		$zip = new ZipArchive();
		$zip->open($source, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		// Create recursive directory iterator
		/** @var SplFileInfo[] $files */
		$files = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator($rootPath),
			RecursiveIteratorIterator::LEAVES_ONLY
		);

		foreach ($files as $name => $file)
		{
			// Skip directories (they would be added automatically)
			if (!$file->isDir())
			{
				// Get real and relative path for current file
				$filePath = $file->getRealPath();
				$relativePath = substr($filePath, strlen($rootPath) + 1);

				// Add current file to archive
				$zip->addFile($filePath, $relativePath);
			}
		}

		// Zip archive will be created only after closing object
		return $zip->close();
	}

	public static function Download(){
		$file = 'backend/compressed.zip';
		if(Helper::Zip($file, 'backend/downloads')){
			if(DEBUG){
				echo "<a href=\"$file\">$file</a>";
			} else {
				header("Cache-Control: public");
				header("Content-Description: File Transfer");
				header("Content-Disposition: attachment; filename=$file");
				header("Content-Type: application/zip");
				header("Content-Transfer-Encoding: binary");
				readfile($file);
			}
		}else{
			die('error');
		}
	}
}








