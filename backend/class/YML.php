<?php

class YML{
	

	private $yml;
	
	function __construct($uri,$CatLimit = 0,$ProdLimit = 0) {
	   $this->LoadYML($uri);
	   $this->parsCategories($CatLimit);
	   $this->parsProducts($ProdLimit);   
	}
	
	private function LoadYML($uri){
		$this->yml = simplexml_load_file($uri);		
	}
	
	private function parsCategories($limit = 0){
		$i=1;
		foreach ($this->yml->shop->categories[0] as $cat) {
			$id = (string)$cat->attributes()['id'];
			$parentId = (string)$cat->attributes()['parentId'];
			$name = (string)$cat[0];
			
			$this->AddNewCategory($id,array("name" => trim($name), "pname" => NULL , "pid"=> $parentId));
			
			if($limit !=0 and $limit==$i)
				break;
			$i++;
		}
	}
	
	private function parsProducts($limit = 0){
		$i=1;
		foreach ($this->yml->shop->offers[0] as $prod) {
			if($prod->param->attributes()){
				foreach($prod->param as $p){
					$param[(string)$p->attributes()['name']] =(string)$p;
				}
			}else{
				$param = array();
			}
			Helper::$ProductList[] = new Product((string)$prod->name,(string)$prod->vendorCode,(string)$prod->price,(string)$prod->currencyId,(string)$prod->categoryId,(string)$prod->picture,$param);
					
			if($limit !=0 and $limit==$i)
				break;
			$i++;
		}	
	}
	
	private function AddNewCategory ($id ,$Line){
		Helper::$CategorySortedList[$id] = $Line;
		foreach(Helper::$CategorySortedList as $k => $s){
			if ($s['pid'] != NULL and $s['pname'] == NULL){
				if (array_key_exists($s['pid'], Helper::$CategorySortedList)) {
					$pline = Helper::$CategorySortedList[$s['pid']];
					$s['pname'] = $pline['name'];
					Helper::$CategorySortedList[$k] = $s;
				}
			} 
		}
	}	
}