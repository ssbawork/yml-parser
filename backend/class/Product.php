<?php

class Product{
	
	public $price;
	public $category;
	public $picture;
	public $pictureURI;
	public $name;
	public $code;
	public $params;

	function __construct($name,$code,$price,$currency,$catid,$picuri,$param) {
		$this->SetPrice($price,$currency);
		$this->SetCategory($catid);
		$this->SetPicture($picuri);
		$this->SetName($name);
		$this->SetCode($code);
		$this->SetParams($param);		
		
                
		file_put_contents("backend/downloads/img/".$this->picture, fopen($this->pictureURI, 'r'));
		
	}
	
	public function SetPrice($price,$currencyId){
		$this->price = $this->convertCurrency($price, "RUB", $currencyId);
	} 
	
	public function SetCategory($categoryId){
		$this->category = Helper::$CategorySortedList[$categoryId]['name'];
	} 
	
	public function SetPicture($picURI){
		$this->pictureURI = $picURI;
		$picURI = explode("/",$picURI);
		$this->picture = end($picURI);
	} 		

	public function SetName($name){
		$this->name = $name;
	} 	
		
	public function SetCode($code){
		$this->code = $code.rand(100,999);
	} 	
				
	public function SetParams($params){
		$this->params = $params;
	} 
		
	private function convertCurrency($amount, $from, $to){
		return round($amount * Helper::getRate($from.$to),0);
	}
	
}
