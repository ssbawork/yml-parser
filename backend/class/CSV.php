<?php

class CSV{

	function __construct($CategorySortedList, $ProductList) {
	
		$this->SaveCategoriesCSV($CategorySortedList);
		$this->SaveProductCSV($ProductList);
		
	}

	private function SaveCategoriesCSV($CategorySortedList){
		$output = fopen('backend/downloads/Categories.csv', 'w+');
		ksort ($CategorySortedList);
		foreach($CategorySortedList as $s){	
				fputs($output,implode(array('', $s['name'], $s['pname']), ';').";\n");
		}
	}

	private function SaveProductCSV($ProductList){
		$output = fopen('backend/downloads/Products.csv', 'w+');
		fputs($output,implode(array("product_id", "ean", "qty", "date", "price", "tax", "category", "name", "short_description", "description", "product_thumb_image", "product_name_image", "product_full_image"), ';').";\n");
		foreach($ProductList as $single){	
			fputs($output,implode(array("", $single->code, "", "", $single->price, "", $single->category, $single->name, "", "", "", $single->picture, ""), ';').";\n");
		}
	}
}