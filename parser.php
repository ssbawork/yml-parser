<?php
ini_set('max_execution_time', 3000);
//error_reporting(0);

include("backend/settings.php");
include("backend/helper.php");
include("backend/class/Product.php");
include("backend/class/YML.php");
include("backend/class/CSV.php");


$yml = new YML(YMLURI,0,0);

if(DEBUG){
	echo "<pre>";
	print_r(Helper::$RateList);
	echo "<br/>";
	print_r(Helper::$ProductList);
	echo "<br/>";
	print_r(Helper::$CategorySortedList);
}

$csv = new CSV(Helper::$CategorySortedList, Helper::$ProductList);
Helper::Download();


?>