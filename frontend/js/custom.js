$('#StartButton').on('click', function(event) {
	$("#subtitle").text("В процессе");
	$("#CountDownTimer").TimeCircles({ time: { Days: { show: false }, Hours: { show: false } }});
	$("#StartButton").hide();
	$.ajax({
	  url: 'parser.php',
	  complete: function(){
		$("#CountDownTimer").TimeCircles().stop();
		$("#subtitle").text("Завершено");
	  }
	});
});  